﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour {

    NavMeshAgent agent;
    public GameObject target;
    public WASDMovement playerMovement;
    public Agent agentType;
    public float range;
    
    void Start() {
        agent = GetComponent<NavMeshAgent>();   
        playerMovement = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location){
        agent.SetDestination(location);
    }

    void Flee(Vector3 location){
        Vector3 fleeDirection = location - transform.position;
        agent.SetDestination(transform.position - fleeDirection);
    }

    void Pursue(){
        Vector3 targetDirection = target.transform.position - transform.position;
        float lookAhead = targetDirection.magnitude/(agent.speed + playerMovement.currentSpeed);
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    void Evade() {
        Vector3 fleeDirection = target.transform.position - transform.position;
        float lookAhead = fleeDirection.magnitude/(agent.speed + playerMovement.currentSpeed);
        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    void Wander(){
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        Vector3 wanderTarget = new Vector3(Random.Range(-1f, 1f) * wanderJitter, 0, Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget = wanderTarget.normalized ;

        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    void Hide(){
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        //int hidingSpotsCount = World.Instance.HidingSpots.Length;
        foreach (GameObject obstacles in World.Instance.HidingSpots) {
            
            Vector3 hideDirection = obstacles.transform.position - target.transform.position;
            Vector3 hidePosition = obstacles.transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(transform.position, hidePosition);
            if (spotDistance < distance){
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        Seek(chosenSpot);
    }

    void CleverHide(){
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDirection = Vector3.zero;
        GameObject chosenGameObject = World.Instance.HidingSpots[0];

        //int hidingSpotsCount = World.Instance.HidingSpots.Length;
        foreach (GameObject obstacles in World.Instance.HidingSpots) {
            
            Vector3 hideDirection = obstacles.transform.position - target.transform.position;
            Vector3 hidePosition = obstacles.transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(transform.position, hidePosition);
            if (spotDistance < distance){
                chosenSpot = hidePosition;
                distance = spotDistance;
                chosenDirection = hideDirection;
                chosenGameObject = obstacles;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDirection.normalized);
        RaycastHit info;
        float rayDistance = 100f;
        hideCol.Raycast(back, out info, rayDistance);

        Seek(info.point + chosenDirection.normalized * 5);
    }

    bool CanSeeTarget() {
        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - transform.position;
        if(Physics.Raycast(transform.position, rayToTarget, out raycastInfo)){
            return raycastInfo.transform.gameObject.tag == "Player";
        }
        return false;
    }

    // Update is called once per frame
    void Update() {
        if(Vector3.Distance(transform.position, target.transform.position) > range){
            Wander();
            return;
        }

        switch (agentType){
            case Agent.Agent1:
                Pursue();
                break;
            case Agent.Agent2:
                if(CanSeeTarget()){
                    CleverHide();
                }
                break;
            case Agent.Agent3:
                Evade();
                break;
        }
    }
}

public enum Agent{
    Agent1, Agent2, Agent3
};
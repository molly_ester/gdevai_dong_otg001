﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    public Transform player;
    NavMeshAgent agent;

    float spd;

    void Start(){
        agent = GetComponent<NavMeshAgent>();
        spd = agent.speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, player.position) <= 1f){
            agent.speed = 0;
        }
        else agent.speed = spd;
    }
}

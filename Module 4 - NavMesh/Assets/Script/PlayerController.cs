﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Move : UnityEvent<PlayerController> { }
public class PlayerController : MonoBehaviour {

    public CharacterController controller;

    public float spd = 12f;
    public float grav = -9.8f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 curPos;

    Vector3 velocity;
    bool isGrounded;

    public Move onMove = new Move();



    // Update is called once per frame
    void Update() {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0) {
            velocity.y = -2f;
        }

        Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        velocity.y += grav * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }

    void Move(float x , float z){
        if (x == 0 && z == 0) return;
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * spd * Time.deltaTime);
        onMove.Invoke(this);
    }
}

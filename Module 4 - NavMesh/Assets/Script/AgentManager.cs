﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentManager : MonoBehaviour
{
    public Transform playerPosition;
    PlayerController player;

    public NavMeshAgent[] agents;

    void Start() {
        player = playerPosition.gameObject.GetComponent<PlayerController>();
        foreach (NavMeshAgent ai in agents) {
            ai.gameObject.GetComponent<AI>().player = playerPosition;
            player.onMove.AddListener((controller) => ai.SetDestination(playerPosition.position));
            ai.SetDestination(playerPosition.position);
        }
        
    }
}

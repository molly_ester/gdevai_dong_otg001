﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour {
    Transform player;
    float spd = 2f;
    float rotSpd = 2.5f;

    void Start() {
        player = GameObject.FindWithTag("Player").transform;

    }

    // Update is called once per frame
    void LateUpdate() {
        Vector3 pos = new Vector3(player.position.x, transform.position.y, player.position.z);
        Vector3 dir = pos - transform.position;



        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), rotSpd * Time.deltaTime);

        if(Vector3.Distance(pos, transform.position) > 1)
            //transform.Translate(0, 0, spd * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, pos, spd * Time.deltaTime);

    }
}

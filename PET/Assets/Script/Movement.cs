﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    float h, v;
    GameObject head;
    Camera eyes;
    Vector2 mouseCurPos;

    [Range(1, 20)]
    [SerializeField] float spd;




    void Start() {
        head = transform.GetChild(0).gameObject;
        eyes = head.GetComponent<Camera>();

        mouseCurPos = Input.mousePosition;
        Cursor.visible = false;
    }

    void LateUpdate()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        transform.Translate(new Vector3(h * spd * Time.deltaTime, 0, v * spd * Time.deltaTime));

        MouseLook(Input.mousePosition);
    }

    void MouseLook(Vector2 pos){
        if (pos != mouseCurPos){
            Vector2 rot = mouseCurPos - pos;
            mouseCurPos = Input.mousePosition;
            
            float yAxis = head.transform.eulerAngles.x + rot.y;
            yAxis = yAxis > 90 && yAxis < 180 ? 90 : yAxis;
            yAxis = yAxis < 270 && yAxis > 180 ? 270 : yAxis;

            head.transform.eulerAngles = new Vector3(yAxis, head.transform.eulerAngles.y, head.transform.eulerAngles.z);//head.transform.eulerAngles + new Vector3(+rot.y, 0,0);
            //head.transform.eulerAngles = new Vector3(Mathf.Clamp(head.transform.eulerAngles.x, 90-360, 90), head.transform.eulerAngles.y, 0);
            transform.eulerAngles = transform.eulerAngles + new Vector3(0, -rot.x, 0);
        }
    }
}
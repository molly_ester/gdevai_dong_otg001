﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankManager : MonoBehaviour
{
    public GameObject[] tanks;
    GameObject selectedTank;

    void Start() {
        selectedTank = tanks[0];
    }
    public void SelectTank(int i){
        selectedTank = tanks[i];
    }

    public void SetDestination(int i){
        Debug.Log(selectedTank);
        selectedTank.GetComponent<FollowPath>().GoToDestination(i);
    }
}

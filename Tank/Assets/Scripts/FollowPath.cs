﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour {
    
    float spd = 5;
    float accuracy = 1;
    float rotSpd = 2;

    Transform goal;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int curIndex = 0;
    Graph graph;

    void Start(){
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];
    }

    void LateUpdate(){
        if(graph.getPathLength() == 0 || curIndex == graph.getPathLength()){

            return;
        }

        currentNode = graph.getPathPoint(curIndex);

        if (Vector3.Distance(graph.getPathPoint(curIndex).transform.position,
                                transform.position) < accuracy) { 
            curIndex += 1;
        }
        
        if(curIndex < graph.getPathLength()){
            goal = graph.getPathPoint(curIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 dir = lookAtGoal - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * rotSpd);
            transform.Translate(0, 0, spd * Time.deltaTime);
        }
    }

    public void GoToDestination(int x){
        graph.AStar(currentNode, wps[x]);
        curIndex = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    Animator anim;
    public GameObject player;
    public GameObject bullet;
    public GameObject turret;
    Health health;

    void Start(){
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
    }
    
    void Update(){
        anim.SetFloat("Distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetFloat("Health", health.Ratio);
    }

    public void StopFiring(){
        CancelInvoke("Fire");
    }

    public void StartFiring(){
        InvokeRepeating("Fire", .5f, .5f);
    }

    void Fire(){
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }
}

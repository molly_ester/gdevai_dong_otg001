﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTank : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;

    void Fire(){
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    // Update is called once per frame
    void Update() {
        if(Input.GetMouseButtonDown(0)){
            Fire();
        }
    }
}

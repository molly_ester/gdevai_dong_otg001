﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] float maxHealth;
    float health;
    
    public float Ratio => health/maxHealth;

    void Start(){
        health = maxHealth;
    }
    public void TakeDamage(int damage){
        health -= damage;
        health = Mathf.Max(health, 0);
        
        if(health <= 0){
            gameObject.SetActive(false);        
        }
    }
}

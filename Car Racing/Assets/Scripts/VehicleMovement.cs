﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class VehicleMovement : MonoBehaviour
{
    [SerializeField] Transform goal;
    [SerializeField] float minSpd, spd, maxSpd;
    [SerializeField] float rotSpd = 5;
    [SerializeField] float accuracy = 1;
    [SerializeField] float decceleration = 2;
    [SerializeField] float acceleration = 5;
    [SerializeField] float breakAngle;

    [SerializeField] float deccelerationThreshold;

    [SerializeField] WaypointCircuit circuit;

    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
        Vector3 dir = lookAtGoal - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), rotSpd * Time.deltaTime);

        if (Vector3.Angle(goal.forward, transform.forward) > breakAngle && spd > deccelerationThreshold) {
            spd = Mathf.Clamp(spd - (decceleration * Time.deltaTime), minSpd, maxSpd);
            
        }
        else{
            spd = Mathf.Clamp(spd + (acceleration * Time.deltaTime), minSpd, maxSpd);
        }


        transform.Translate(0, 0, spd);

        

    }
}

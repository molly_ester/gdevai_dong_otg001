﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{
    public GameObject[] waypoints;

    int currentWaypointIndex;

    float spd = 5;
    float rotSpd = 3;
    float accuracy = 1;
    // Start is called before the first frame update
    void Start()
    {
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(waypoints.Length <= 0) return;

        GameObject currentWaypoint = waypoints[currentWaypointIndex];
        Vector3 lookAtGoal = new Vector3(currentWaypoint.transform.position.x, transform.position.y, currentWaypoint.transform.position.z);

        Vector3 dir = (lookAtGoal - this.transform.position).normalized;

        if (Vector3.Distance(transform.position, lookAtGoal) < 1f){
            currentWaypointIndex++;
            currentWaypointIndex = waypoints.Length <= currentWaypointIndex ? 0 : currentWaypointIndex;
        }

        transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * rotSpd);

        transform.Translate(0, 0, spd * Time.deltaTime);

    }
}
